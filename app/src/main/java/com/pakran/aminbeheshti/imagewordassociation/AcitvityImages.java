package com.pakran.aminbeheshti.imagewordassociation;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AcitvityImages<levelCounter> extends AppCompatActivity {

    GridView gridView;
    List<Integer> listOfInt = new ArrayList<>();
    private String answerOfTest;
    private int level;
    private int clickCounter=0;
    int second;

    String[] names = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
    int[] images = {R.drawable.ic01, R.drawable.ic02, R.drawable.ic03, R.drawable.ic04, R.drawable.ic05,
            R.drawable.ic06, R.drawable.ic07, R.drawable.ic08, R.drawable.ic09, R.drawable.ic10, R.drawable.ic11,
            R.drawable.ic12, R.drawable.ic13, R.drawable.ic14, R.drawable.ic15,
            R.drawable.ic16, R.drawable.ic17, R.drawable.ic18, R.drawable.ic19, R.drawable.ic20};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            answerOfTest = (String) b.get("arg2");
            level = (int) b.get("level");
            second = (int) b.get("second");
            setLevel(level);
            Log.d("kkkkkkkkkkkkkk", String.valueOf(answerOfTest));
            Log.d("tttttttttttttt", String.valueOf(answerOfTest.length()));

        }
        gridView = findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter(names, images, this);
        gridView.setAdapter(customAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listOfInt.add(position + 1);
                checkTheResult(getAnswerOfTest(),String.valueOf(listOfInt));
                clickCounter++;
            }
        });
    }

    public class CustomAdapter extends BaseAdapter {

        private String[] imageNames;
        private int[] imagesPhoto;
        private Context context;
        private LayoutInflater layoutInflater;

        public CustomAdapter(String[] imageNames, int[] imagesPhoto, Context context) {
            this.imageNames = imageNames;
            this.imagesPhoto = imagesPhoto;
            this.context = context;
            this.layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return imagesPhoto.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.row_items, parent, false);
            }

            TextView tvName = convertView.findViewById(R.id.tvName);
            ImageView imageView = convertView.findViewById(R.id.imageview);

            tvName.setText(imageNames[position]);
            imageView.setImageResource(imagesPhoto[position]);
            return convertView;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void checkTheResult(String answerOfTest, String answerOfTastTaker) {
        Log.d("result:", String.valueOf(listOfInt));
        if (Objects.equals(answerOfTest,answerOfTastTaker)) {
            level++;
            Intent intent = new Intent(getApplicationContext(), ActivityWin.class);
            intent.putExtra("levelCount", getLevel());
            intent.putExtra("second", second);
            startActivity(intent);

        }else if (clickCounter == level+2){
            level=0;
            Intent intent = new Intent(getApplicationContext(), ActivityLose.class);
            startActivity(intent);
            Log.d("sssssssssssssssss","nashodddddddddddddddd");}
    }
    public String getAnswerOfTest() {
        return answerOfTest;
    }
    public int getClickCounter(){
        return clickCounter;
    }
    public int getLevel(){return level;}
    public void setLevel(int level){this.level = level;}
    @Override
    public void onBackPressed() {
    }
}