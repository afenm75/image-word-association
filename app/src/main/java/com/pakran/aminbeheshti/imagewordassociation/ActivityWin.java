package com.pakran.aminbeheshti.imagewordassociation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityWin extends AppCompatActivity {
    static int level;
    int second;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);

        TextView textWin = findViewById(R.id.text_win);
        Intent iin = getIntent();
        Bundle b = iin.getExtras();

        if (b != null) {
            level = (int) b.get("levelCount");
            setLevel(level);
            second = (int) b.get("second");
        }

        textWin.setText("Level " + getLevel() + " is completed");
        Button btn = findViewById(R.id.button_Next_level);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivty();
            }
        });

    }
    public void openMainActivty(){
        Intent intent = new Intent(getApplicationContext(), ActivityOfFading.class);
        intent.putExtra("levelCounter", getLevel());
        intent.putExtra("second", second);
        Log.d("ssssssssssssssss", String.valueOf(level))  ;
        startActivity(intent);
    }

    public static void setLevel(int level) {
        ActivityWin.level = level;
    }

    public static int getLevel() {
        return level;
    }
    @Override
    public void onBackPressed() {
    }
}
