package com.pakran.aminbeheshti.imagewordassociation;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.tomer.fadingtextview.FadingTextView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ActivityOfFading extends AppCompatActivity {

    List<Integer> listOfInt = new ArrayList<>();
    private String[] example2;
    private int second;
    private int level;
    String[] salutation = {"Chair","Banana","Phone","spoon","Table","Apple","House","Monkey","Bed","Cow",
            "Plate", "Pants","Water","Glasses", "Orange","Earth","T shirt","Hat","Shoes", "Flower"};
    private FadingTextView fadingTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_of_fading);
        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            int j =(int) b.get("second");
            level = (int) b.get("levelCounter");
            setSecond(j);
            setLevel(level);
        }
        fadingTextView = findViewById(R.id.fading_text_view);
        example2 = setWords(getLevel());
        fadingTextView.setTexts(example2);
        fadingTextView.setTimeout(getSecond()*500, TimeUnit.MILLISECONDS);
        Button buttonApply = findViewById(R.id.startgame);
        buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             openActivtyImages();
            }
        });
    }

    public void openActivtyImages(){
        Intent intent = new Intent(getApplicationContext(), AcitvityImages.class);
        intent.putExtra("arg2", String.valueOf(getListOfInt()));
        intent.putExtra("second",getSecond());
        intent.putExtra("level",getLevel() );
        Log.d("ssssssssssssssss", String.valueOf(getListOfInt().toArray()[0]))  ;
        startActivity(intent);
    }

    //    public void startExample2(View v) {
////        String[] example2 = {"And", "this", "is", "example 2"};
//        String[] example2 = {salutation[0],salutation[1], salutation[2]};
//        fadingTextView.setTexts(example2);
//        fadingTextView.setTimeout(300, TimeUnit.MILLISECONDS);
////        fadingTextView.forceRefresh();
//    }
    public String[] setWords(int level){
        String[] s = new String[100];
        s = new String[]{"1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"};
        List<String> list = new ArrayList<>();
        Random r=new Random();
        for(int i =0; i<3+getLevel(); i++) {
//            int randomNumber1 = r.nextInt(salutation.length);
//            int randomNumber2 = r.nextInt(salutation.length);
            int index=0;
            int previousIndex = index;
            index = r.nextInt(salutation.length);

            while (index == previousIndex)
            {
                index = r.nextInt(salutation.length);
            }
            list.add(salutation[index]);
            listOfInt.add(index+1);
        }
        s= list.toArray(s);
        return s;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second){
        this.second = second;
    }

    public int getLevel(){ return level; }

    public void setLevel(int level){
        this.level=level;
    }

    public String[] getExample2() {
        return example2;
    }

    public List<Integer> getListOfInt() {
        return listOfInt;
    }
    @Override
    public void onBackPressed() {
    }
}