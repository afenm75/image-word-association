package com.pakran.aminbeheshti.imagewordassociation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    RadioButton radioButton;
    TextView textView;
    private int second;
    private int level=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup = findViewById(R.id.radioGroup);
//        textView = findViewById(R.id.text_view_selected);
        Button buttonApply = findViewById(R.id.button_apply);
        buttonApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkButton(v);
                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(radioId);
                openActivtyOfFading();
            }
        });
    }

    public void openActivtyOfFading(){
        Intent intent = new Intent(this, ActivityOfFading.class);
        intent.putExtra("second", getSecond());
        intent.putExtra("levelCounter", getLevel());
        startActivity(intent);
    }

    public void checkButton(View v) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);
        if (radioButton.getText() == "One"){
            second = 2;
        }else if(radioButton.getText() == "Two"){
            second = 4;
        }else second=6;
    }

    public int getSecond() {
        return second;
    }

    public int getLevel(){return level;}

    public void setLevel(int level){
        this.level = level;
    }
}

