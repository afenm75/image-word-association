package com.pakran.aminbeheshti.imagewordassociation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityLose extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_lose);

        Button btnTry = findViewById(R.id.button_try_again);
        btnTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivty();
            }
        });
    }

    public void openMainActivty(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        intent.putExtra("arg2", String.valueOf(getListOfInt()));
//        Log.d("ssssssssssssssss", String.valueOf(getListOfInt().toArray()[0]))  ;
        startActivity(intent);
    }
    @Override
    public void onBackPressed() {
    }
}
